Command Line Cheatsheet
=======================

Running command:

    command <important> [optional]    # comment

Legend:
* `command` is to be typed literally
* `<important>` is to be included appropriately
* `[optional]` can be omitted, but can be useful
* `# comment` is for human only; terminal ignores it

**NOTE**: Multiple spaces are treated as a single space

Special symbols for directory
* Forward-slash ("/"): path delimiter
* Single dot ("."): current directory
* Double dot (".."): parent directory
* Tilda ("~"): home directory
* Star ("\*"): wildcard (multi-char)
* Question mark ("?"): wildcard (single-char)

Root directory ("/"): base directory, no name, called with a single forward-slash

Home directory ("~"): where your user files live; usually /home/<user>/

Current/working directory ("."): where your terminal is currently pointing at

View current directory path:

    pwd

Changing current directory to home (or [dir] if specified):

    cd [dir]

Listing files in current directory (or [dir1, dir2, dir3, …] if specified):

    ls [dir1, dir2, dir3, …]

Copying <src1> (also optionally [src2, src3, …]) to <dest>

    cp <src1> [src2, src3, …] <dest>

Moving <src1> (also optionally [src2, src3, …]) to <dest>

    mv <src1> [src2, src3, …] <dest>

Removing <file1> (also optionally [file2, file3, …])

    rm <file1> [file2, file3, …]

Getting help for <command>:

    man <command>    # press Q to quit
    <command> --help

Convenience
* Line: ArrowLeft, ArrowRight, Home, End, Del
* History: ArrowUp, ArrowDown
* Auto-complete: Tab (with partially typed)
* Scroll (per line): Ctrl+Shift+ArrowUp, Ctrl+Shift+ArrowDown
* Scroll (per page): Ctrl+Shift+PageDown, Ctrl+Shift+PageDown

-- END
