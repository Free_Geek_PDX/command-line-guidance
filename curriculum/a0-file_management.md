Introduction
===============

## What will be covered?
This class intends to introduce the basic parts of a command, listing, copying, moving, linking, tab completion, file system structure, and more, for the Linux command line.

- What is the command line, and why you use it 
- Basic syntax and rules for typing in command line
- Basic commands needed to navigate and manage files and directories
- Where to get more resources for learning command line

When finished you should have the basic tools needed to pursue more advanced investigation of the features of the Linux CLI such as Administration and configuration of your system, shell scripting, and other advanced topics.

## What is the CLI? 
The __CLI__(Command Line Interface) is a tool for typing computer commands, instead of clicking menus or buttons. It was the historic way to use a computer before the __GUI__ (Graphic User Interface) was developed.

- A text interface to your computer
- An alternative to Windows’ File Explorer or the Mac Finder on OS X
- Accepts and executes commands
- A command is a directive to the computer to perform a certain task
- List files, move files, rename files, delete files, create folders, etc. The files and folders on your    computer are organized into what’s called a filesystem.
- Does much more than the GUI



## Why learn about the CLI?
Learning the __CLI__ is a great way to get familiar with core concepts of programming and in many cases make use of more advanced features that may not have a __GUI__.

- CLI commands are necessary when scripting in Linux
- Often more powerfull than the GUI
- Run same programs as the GUI but with more options
- Commands may be combined (or chained) to perform sophisticated tasks
- Connect to remote computers
- Troubleshooting
- Check the status of the system
- Check how much memory, CPU and storage is being used
- Check logs
- Perform maintenance
- Configure services
- Start and stop services
- Perform auxiliary tasks around software development
- Run tests on software and your system



Getting Access to the Command Line Interface or __CLI__
===============
There are three ways to open a "Terminal" to access the CLI

(try these as we discuss them)

1. Desktop:
  - Right click on your desktop  
  - Select "Open in Terminal"  

2. Menu
  - Select the "Menu" item in the bottom panel  
  - Select the "Terminal" icon in the left column  

3. Panel
  - Select the "Terminal" icon in the bottom panel if it exists

Note: The Terminal Icon is a Black icon with white border showing a greater than symbol and an underscore.

- If you try all three of these techniques you will open three "terminals" but they may overlay one another. Use your mouse/trackpad to "unstack" them.
- All these will open a "Terminal" that allows you to enter commands that are executed by the "Shell" program. In Linux the Shell program is "Bash". (Others are csh, ksh, zsh,... but we'll stick with bash.)
- Opening the Terminal is sometimes referred to as "opening a shell".

## Terminal controls

Terminal window (right side of the title bar)
  - Click the plus sign to maximum the window to maximum size
  - Click the minus sign to put the window away
  - Click the X to close the terminal application  

Font size
 - CTRL + SHIFT + “=”
 - CTRL + “-”

## Menu bar controls

We won't discuss all of these

 - File
 - Edit
 - View
 - Search
 - Terminal
 - Help
 
 But 

- "Edit" items helps you copy and paste text you have highlighted using the mouse/track pad
- "View" lets you resize the window and the text


## The Terminal Prompt
What is the prompt? 

- A prompt appears when the shell is ready to accept a command
- It appears on the line following the output of a command

The parts of the prompt:

1. Green Part
  - The username and machine name separated by "@" (e.g. “guest-UGu6Yb@shylock” or "ericb@unicorn")  

2. Blue Part
  - The tilde appears in the prompt location reserved to show you the directory you are currently in
  - A dollar sign: the end of the prompt

3. The cursor
  - showing where your typed command will appear

The Tilde (__~__) - represents your __home__ directory
- This is your workspace in which to create files and other directories
-  Only you have permission to create files here
- The tilde symbol is a shortcut for your home directory

**NOTE:** The Tilde only appears because you are you home directory, it will be replaced by the path to the directory you are currently in when that changes



Directory Structures and File Management
===============
The Linux file system is a hierarchy
- Similar to Windows and the same as OS X and consists of files and directories.
- At the top is the __root__ of the file system designated as "/"
- A directory is the same thing as a folder. On the command line, we refer to folders as “directories”
- A directory may contain files as well as other directories
-- We call a directory contained by another directory a "sub-directory"
- Any other slash serves as an indicator to show the parts of the hirearchy

## The strings that specify the location of a file/directory is call its __path__
Consider a directory containing albums by Led Zeppelin and the tunes in each
```
/home/ericb/music/led_zeppelin/coda/walkers_walk.mp3
/home/ericb/music/led_zeppelin/coda/poor_tom.mp3
/home/ericb/music/led_zeppelin/destroyer/no_quarter.mp3
/home/ericb/music/led_zeppelin/destroyer/kashmir.mp3
...
```

```
/
 home
   ericb
     music
       led_zepplin
         coda
           walkers_walk.mp3
           poor_tome.mp3
         destroyer
           no_quarter.mp3
           kashmir.mp3
```

### Special symbols for directory

Short hand references for navigating and referring to file paths
  -  Forward-slash ("/"): path delimiter (or root directroy if it is the first item in a file path)
  -  Tilde ("~"): home directory
  -  Single dot ("."): current directory
  -  Double dot (".."): parent directory

## Your Home directory In the CLI and GUI

### The desktop, in Linux Mint, provides a link to __Nemo__  which is similar to the Windows "File Explorer" or the OS X/Mac "Finder"

  - Double click on the "Home" icon on the desktop to start __Nemo__ there
  - The display in the title bar at the top shows that you are looking at the directory called "Home" 
  - All users on your system will have a subdirectory created in "/home"
  - The two panels show the directories available and by default the contents of your personal home directory named after your userid

### When you open the "Terminal" to use the CLI it places you in the same "Home" directory as Nemo

  - Open a terminal window
  - Type your first command, the "list" command, by entering  
    __ls__
  - Note that the output corresponds to most of what you see in Nemo
  - We'll explore the "ls" command more as we go.


## Where are we in the file system

- To see where we are in the file system you use the "Print Working Directory" command to show the current working directory.  
    __pwd__

- This will return  
    __/home/user_name__

- As mentioned before, the tilde (__~__) represents this location and can be used as a shorthand.


## Directory structure Review

- Recall that the first, slash "/" represents the "root directory", often called a  "folder" in the Windows world. It contains all the other files and directories 

- /home  contains the directory created for your login account 
- /home/user_name  is your personal home directory
- Creating a new directory named "new" in your home directory will appear as  
  /home/user_name/new
- Each directory may contain both files and directories


## Commands and their syntax

- The terminal window allows you to "talk" to a program called a __shell__ which is a command interpreter. In Linux Mint it is the __bash__ shell 
- The shell reads the commands you type and performs the function requested by that command.
- The output is either a message, an action with no message, or both a message and an action.
- If no message results from a command it indicates it has successfully completed the command
- All commands follow the same syntax/structure

```
    command -options [arg1 arg3 ...]    # comment text

Legend:  

    command         - is to be typed literally  
    -options        - are flags that change the way the basic command operates, denoted by a leading "-"
    [arg1 arg2 ...] - are arguments that may be optional, the number depending on the command  
    # comment       - is for human only; terminal ignores it  
```
- Options control difference actions the command may perform like modifying the output or providing detail directions for processing.

NOTE: Multiple spaces are treated as a single space

### Two types of Commands

- built-in  - that is they are part of the shell 
- External  - stand alone programs that reside, typically, in /bin or /sbin or other special locations in the file system. These are found using a "shell variable" called __$PATH__

## ls - Look at more details of your files and directories

- The __ls__ command has several important options
- __ls -a__ lists all the contents of the current directory
- __ls -l__ creates a "long" listing showing details about the current directory
- __ls -t__ sort by creation time
- __ls -r__ reverse the sort order
- __ls -latr__ you can combine options to get specific listing styles  __<== NOTE__

Look at the output of the __ls -l__ command:

- __Type indicator__: "d" directory "-" file "l" link, etc. - more later
- __Access bits__: who can access the file/directory - more later
- __Number of links__: second column--we'll discuss this later
- __Owner__: the next two columns--respectively __user__, then __group__
- __Size__: follows the Owner and shows the size in bytes
- __Time of last modification__

Now look at __ls -la__

### Hidden Files

- Files/directories that begin with a "." are "hidden" and do not appear with __ls -l__. 
- They are commonly used for configuration files
    - define/store parmeters for your user interface (.conf .gconf .cinnamon .profile .bashrc etc.) 
    - define/store parameters used for programs like browsers, editors or other programs (Thunderbird, Firefox, Gimp, etc.)
    - store persistent data between sessions

Note also that "." current directory and ".." parent directory show up.

## man - Learning the details of the commands

Linux has a built in manual page that can be accessed using the __man__ command.
    man ls
- Search using "/text"
   - Try "man ls" and search for -t  using "/-t"
   - Type an "n" for the next "found" entry
- Up and Down arrows
- Right and Left arrows
- Page Up, Page Down
- Home key goes to top of the page
- Enter: advances the page a line at a time
- Space: advances the page a full page at a time
- Type a "q" to exit the "man page"

The internet is filled with pages that will provide details of the use of commands and suggest commands and options it use for just about any operation.

## Basic Operations

- pwd
- ls
- cd
- cp, mv, rm
- mkdir, rmdir, rm -r
- . .. ~ and tab continuation

Each time you run a command, check your directory (with *ls -la*) to see the changes.

### Copy a file / Absolute and Relative Referencing (cp)
    
    ls debian*      # no files found
    cp /etc/debian_version .
    
What does *ls* show you?

You can do a copy to a different name:

    cp /etc/debian_version foo_version

Or

    cp /etc/debian_version ./bar_version
    

Note that the current directory (denoted with '.') is mentioned explicitly or simply not mentioned at all. This is called __relative__ addressing (location relative to current).

Use the "wild card" __*__ to see the files we just created

    ls *version
    
[For more information on wild cards *,? and [...], see  *man bash* and search for "Pattern Matching"]

Now let's use the __absolute__ path to copy a file 

    cp foo_version /tmp
    cp ./bar_version /tmp

/tmp is a directory available to hold files/directories temporarily. Don't rely on it between sessions but it's good for temporary use.


### Tab completion

How can we shorten the amount of typing and copy "debian_version" to /tmp? Use the "Tab completion" feature

    cp deb<tab>

and you should see  "cp debian_version "  
.... and can complete the command typing "/tmp"

If typing a single <tab> does not show an entry, typing a second <tab> will show you all the entries in the current directory that match what you have entered. For example

    ls Do<tab><tab> 

will show at least "Documents" and "Downloads", and re-enter your command as you typed it, allowing you to add more characters to the name and use <tab> again to complete.


### Changing to a Different directory (cd)

Use __cd__ (change directory) to change to the */tmp* working directory

    pwd
    cd /tmp

Here we used the "/" to refer to the "root" directory and a sub-directory in it called "tmp".  Recall this is an *absolute* address.

- What does "pwd" show for your current directory?
- What does "ls -l" show?
- What happened to your prompt? (~)
- What do you see if you use "ls ~" ?
- What do you see if you use "ls .." ?


### Make a new directory (mkdir)
At this point, you should be in the "/tmp" directory. Lets make some new directories using a *relative* address.

    pwd
    mkdir my_dir
    mkdir /tmp/later_dir   # absolute addressing
    
    ls my_ dir
    
### Copy a file using absolute addressing

    cp /etc/debian_version /tmp/my_dir/stop_file
    
List all the directories created again:

    ls *_dir   

This shows the two directories and the new contents of my_dir

    
### Move a file to your directory (mv)
We see how to copy, but what if we only want to move the existing files? 
One way would be to copy the file then erase the old one, but there is a better, more efficient way. This is the **mv** or *move* command.

    ls *_version
    mv foo_version my_dir
    

Now check out the contents of your directory

    ls -l my_dir   # shows one
    ls -l *_dir    # shows both

### Move more than one file
 be cautious when moving more than one file using a wild card. Check to see what the wild card will match with an "ls"

    ls *_version
    mv *_version my_dir
    
    cd my_dir
    ls 
    



With the same "mv" command, you can rename files

    mv foo<tab> odd_version

What files are there now?

    ls 
    
## More on the cd command ##

Say, you are done here and want to move back to your home directory.
Us another short cut, the "~" tilde character.

    cd ~
    pwd

which puts you back in your home directory.

If you want to return to the last directory you were in you can use the "-" short cut.

    cd -
    pwd     #which puts you back in /tmp/my_dir

You can also simply say:

    cd
    pwd     #back to home
    cd -
    pwd     #back to /tmp/my_dir
 
or
    cd /home/user_name
    pwd     #back to home
    cd -
    pwd     #back to /tmp/my_dir


### Removing Files (rm)

NOTE: Removing files and directories in Linux CLI is IMMEDIATE and PERMANENT. There is no "trash can" from which to recover your data!

Now remove the files you copied 

    cd /tmp/my_dir
    pwd         # verify that you are in /tmp/mydir
    ls *ver*
    rm odd_version

Remove multiple files with verification

    rm -i *version
    
respond with "y" for each request.


### Removing Directories (rmdir)

Now try removing a directory you created earlier
    
    cd /tmp         #navigate to /tmp

    ls *_dir
    rm later_dir

Nope, regular remove won't work on directories (for safety reason)

    rmdir later_dir
    
Now remove the other one

    rmdir my_dir        # It fails!

"rmdir" will **only remove an empty directory**.

    ls my_dir           # Shows a file in that directory
    
### Removing Non-empty Directories (rm -r)

But what if you want to remove the directory and all it's contents? 

You can **use the "-r" option on the "rm" command**.  (-r flag for Recursive)

    rm -r my_dir

Use "man rm" to investigate that option. 

Be very careful with options amd the use of wild cards with the "rm" command.  Consider "rm /*". what could possibly go wrong with that command?



Go back to your home directory and clean up the *version files there.

    cd   (or *cd ~* or *cd /home/user_name*)
    ls *_version
    rm *version
    

## Redirection / Pipes

"Redirection" and "pipes" allows you to control where the input and output of commands.

### Redirection

    >  - directs the output of a command to a file
    <  - directs input from a file to a command
    >> - directs the output and appends it to the data in an existing file or creates it


    cd         # go to home
    ls -la > lookit

To quickly look at the contents you can use __cat__ or __less__

    cat lookit

This dumps the contents to the screen with no control

    less lookit

"less" lets you view the contents of a file under your control via commands:

    space  - scroll forward a page
    Enter or j  - scroll forward a line
    b      - scroll back a page
    k      - scroll back a line
    /text  - search for text
    h      - show the commands available
    q      - stops less and drops you back into the shell
    
    See the man page for many more options

To append data to the same file

    ls  /tmp >> lookit
    less lookit
    
Note the output changes from the "ls -la" output to "ls" output at the end of the file "lookit"

### Redirection (input)

    <  - directs the input of a command to a file

Let's investigate the file *lookit* we created earlier to learn how many files and directories exist in our /tmp directory.

We'll redirect the file lookit into a utility called *wc*

    wc -l < lookit
    
__wc - l__ counts the number of lines in the file.  __man wc__ will give you more options.
    

### Pipes

To connect programs (filters) to each other you us a "pipe" as follows:

    ls -la /tmp | wc -l

Recall __wc - l__ counts the number of lines in the file. 

You can use pipes and redirection together

    ls -la /tmp | wc -l > lookit
    cat lookit
    less lookit
    
Note that the old contents of "lookit" are now replaced by the output from *wc*.

Another example uses a command to look at running processes called "ps" and one to filter the output using "grep"

    ps -aef | grep bash
    
Explore these two commands via "man" and on the web.


## Permission & Ownership

To explore this let's copy a command we've been using; it is located in a system directory

    cp /bin/ls .

By the way, this is where all the **external** commands we've used so far are located!

To execute your newly copied program on the current directory

    ./ls

By default the shell will use the command in the "/bin" directory. The "." period is necessary to refer to the command in the current directory instead of the one at "/bin/ls". 

You can try it with cp too; also try with arguments!  For example "./ls -la" will provide the same output as "ls -la"

Look at the details for this file

    ls -l ls  

Consider the options -r, -a, and -t. Investigate them using "man ls".

### Permissions

These settings appear in the second "field" or "column" in the "ls -l" command output

    drwxrwxr-x
    -rwxrwxr-x

- Leading character indicates a directory if a "d" appears
- the next 9 characters are in permissions grouped in threes
- first 3 are file owner (or user) permissions "u"
- second 3 are group permissions "g"
- last 3 are permissions for all others "o"
- r -stands for read 
- w stands for write
- x stands for execute for files and access for directories
- a stands for "ugo" when used in *chmod*

Permissions are changed with the command __chmod__

    chmod ugo+rwx file_or_directory    # adds the permission
or
    chmod ugo-rwx file_or_directory    # removes the permission

Try changing permission on your copy of "ls" and use "ls -l" to see the change

    chmod go-rwx ls

Try executing the copied 'ls'.

Remove the *execute* bit from the file permission

    chmod a-x ls

Try executing file again.

### Groups / Ownership

View the name of your current logged in user and group

    id

You can change the ownership or group uses the __chown__ command. For example

    chown <your_user>:<your_group> ls   # This is a no-op

This command is most effective for systems where multiple groups exist and you are sharing files, or for administrative work to give users access to features.


## File Sizes

File size is shown using the "ls -l" command

Try (notice the **-h** which provides a "human readable" version):

    ls -lh

Observe the "ls" file you just copied. We are now going to make this file blank by feeding a "null" values into it using (redirection)

    echo '' > ls

Look at the "ls -l" output again!

Notice that the actual size of directories are not displayed on the output. To view the total size of directory content:

    du -sh [dir]

Omitting [dir] will yield the current directory

This recursively counts the size of all the content under the specified directory

## Timestamp

Look at the output of 'ls -l' again, and this time, observe the last columns before the file name.

The timestamp of a file gets updated when the file gets modifed in some fashion.

Change the timestamp using the "touch" command to the current time:

    touch ls

Look again!  "touch" can also be used to create empty files.  Try this with

    touch one
    touch two
    
You can specify the time using *-t*

    touch -t [YY]MMDDhhmm   file_name

note that the MM (month) DD (Date) hh (hour) mm (minute) must be provided.
Now remove those files. [using *rm*]


## File Types ##

File name extensions do not denote file types as they do in Windows. They are just part of the name. In fact, often, programs disregard extension entirely.

Files always include metadata which contains a 'magic number' describing the type of the file

To view the type of file:

    file <file_name>

Try it!

    file /bin/ls
    file /etc/debian_versions
    file /dev/sda
    

## Links

the filesystem doesn't actually care about file paths; all it has is a vast space to put things. While most of this space is free for all, a small chunk is reserved for bookkeeping. Bookkeeping is done by indexing the space used, with each file represented by an entry in a table called an inode.

Every time a file is created, some space gets allocated, a new inode is created, and a file path gets linked to it. Inodes can be linked to more than one path, which are kept track of in the tabel with a number.

The number increases when the path gets linked (file creation or hard link creation), and decreases when one gets unlinked to it or the file is deletion. When deleted the disk space is then available.

Two types of link:
* Hardlink -- this one creates a new directory entry and link it directly to an **inode**; most people don't use this as it may cause complications.
* Softlink -- this one simply creates a new file (in a new inode) containing a symbol that points to the target **path**; also called symbolic link (symlink), use this one!

Try it! Do the following:

    cd ~
    mkdir explore
    cd explore
    cp /etc/debian_version .
    ls -l
    ln -s debian_version slink
    ln debian_version hlink
    ln debian_version h2link
    ls -l

Note the size of hlink compared to slink. Also note the second number in each row and compare it to the number of hard links we created.

Now try a new option on the "ls" command.

    ls -li
    
The new number at the left of each line is the "inode". Note that this is the same for your debian_version file and the hlink file. They are referring to the same location on your disk.
The slink file, however, is a different inode and the size is smaller. This is only a "pointer" to the address at which the original file was created.

Symbolic links can be used across different file systems but hard links cannot.

Symlinks are accessed like regular file.

    ls debian_version
    cat debian_version
    cat slink
    cat hlink

Note that you can see links through the "ls" command (light blue if valid, blinking red if invalid).

Now lets look at removing the links (like regular file)!

    rm h2link
    
If you remove a link, the original file will remain. When you remove a hardlink, the same occurs (but a reference counter is decremented)

    ls -l
    
What happened to the number of links (second field in the "ls" output)?

Now remove the original file

    rm debian_version
    ls -li
    cat hlink
    
Note the indication of a "broken link" but also that the "hlink" we created still contains the contents. Note also the "inode".

Beware the 'deletion' you know of doesn't actually remove the data until the space is needed, in which case it's simply overwritten. For sensitive data, you'd want to use special technique, in which the old data immediately gets overwritten.



