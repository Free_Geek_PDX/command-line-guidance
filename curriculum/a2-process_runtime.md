Process Runtime
===============

Your programs are managed by another program.


## Boot Process

Before time, there was bootloader. It has only one job: run the system image. Yes, your OS comes in an image, the rest is just programs working together.

Below assumes that GRUB is used, but can be adapted for other, less common bootloaders
* Start with the computer off
* Press Shift key repeatedly until a menu screen (not BIOS!) shows up
* Press E to edit boot command
* Press Escape to get back to menu, then press Enter to boot


## Init Process

After the system image is up, the operating system is on duty. This is a good time to run the program number one, responsible for running other programs. It is called the 'init' program, and it literally has an ID of one, thus the name.

Your computer is most likely using what's called SystemD (system daemon); others may instead have the legacy sysvinit, or RC (run command, its modern replacement).

The programs that gets called by init are referred to as daemon, service, or startup program. You can view these (assuming you're using SystemD) by running:

    systemctl

Now, systemd itself actually does much more than just an init, but we'll stop here for now.


## System Time

Speaking of time… The system always keeps time, that's how it keeps track of programs

The command 'sleep' tells the terminal to do nothing for the specified number of seconds. Meanwhile the command 'time' records the time taken for the specified commands to complete execution.

Now try it:

    time { sleep 3 ; }

There would be real, user, and sys time.

Whereas the real time should be close to the number in the argument of 'sleep' command. Both user and sys should be (close to) zero. Also notice that the real time would be the sum of the number in the argument and the other times -- these other times is there because of the background process.

Try it again while running something (e.g. open a manpage, or web browser) simultaneously!


## Runtime Information

To put all these together, we can view the current runtime information from the terminal

Introducing: the table of process

    top

With this you can view information about process.

Notice that every process has its associated ID number (called 'PID') and user that owns it.

To view available RAM on the system:

    free -h

Notice that the '-h' makes the byte information 'human readable'.

Keep in mind that the 'free' column shows only some of the available memory, as the ones under 'buff/cache' column can be reclaimed when necessary (costing performance).


## Process Communication

Processes needs a way to communicate, both to the OS itself and to other processes, hence the concept of Inter Process Communication (IPC). This simply refers to mechanisms the OS provides so processes can share information.

One of the most primitive way is with signal. Everytime you send a signal, the target's execution flow gets halted. It's powerful, so be careful!

First launch a program:

    man kill

This 'man' command would be our target process

Let us open another terminal, in which we find the process ID of our target process

    ps -C man

You will see a table-formatted output; find the number under 'PID' column!

To send a signal:

    kill <PID>

Replace <PID> with the number you saw

Notice that the target process on first terminal stops. This is because the (default) signal you sent is a kill signal.

You can use '-s' option (among many) on the 'kill' command to send different signal. It is not about killing, it's about sending message.

However, since behavior may vary, we won't be covering these other signals.

Now feel free to close the second terminal, or try it with 'killall' command instead!

More information on signals can be found with:

    man 7 signal

Processes also give what's called return value that they give out when they exit. This value reports the status of execution. More on this later!

Another way you can interact with processes is by passing things through files. In this example, we use IO redirection as a form of file.

This command list all of the running program

    ps -e

Consider the command 'grep', which searches through files for matching text. Now open Firefox from your program menu.

Then on your terminal, type:

    ps -e | grep firefox

This command uses 'grep' on the output of 'ps -e' to find match.

You should see some output matching your "search".

More on I/O redirection, check out chapter A1, Text Processing.


## Shell Variable

Everything on terminal is handled as string; including settings, as well as other stuff stored during run time.

You can store things temporarily into variables

    VARIABLE="blablabla"

Do not put any space around the equal sign; otherwise it's treated as a command.

Variable, in shell, is just a part of larger feature called substitution. There's actually a lot more about substitution than just variables, feel free to learn on your own time.

You can now use this variable to do your stuff

    echo $VARIABLE

Use double-quotes if you want the substitution feature to work in quotes -- single-quotes would turn the dollar-sign ('$') symbol into literal character

    echo '$VARIABLE'
    echo "$VARIABLE"

Variables can also be cleared

    unset VARIABLE
    echo $VARIABLE

What do you see?

To view all the variables on your system

    env

These are system variables -- try not to touch any of these

Notice some familiar ones (e.g. PWD, USER, HOME, also the ones you defined)

You can also view the return status of a prior command

    echo $?

Zero means success; anything else means failure.

Some of these variables directly affects your shell execution.

    PS1="anything"

Oh no! You ruined it!

    PATH=

Try running any command, notice the error?

Congratulation, you just messed it up even more!

Just kidding! If you open new terminal window, it will run normally.
