Text Processing
===============

Data have to interpreted somehow. Your terminal is an interpreter -- it takes certain input, and gives appropriate output.


## The UNIX I/O

Everything in UNIX is a file! This includes the stuff in your terminal.

Type

    ls

What you typed in came on standard input (stdin, 0). The printed result came on standard output (stdout, 1).

The numbers above are for file descriptors. Every process has different list of file descriptors, keeping track of files the it accesses. Each time a new file gets opened, a file description gets created with unique ID, pointing to it. Going with the above, 'stdin' has the file ID of 0. Processes, by default, allocate the first three (0, 1, 2) for the terminal.

Let's try an invalid command

    ls djktgheyoptpw7ywp  # or any other nonsense

The error you saw came on standard error (stderr, 2)

This command simply prints out its argument to the terminal

    echo 'print this'
    echo "print this"

The quote is not required, but good habit to keep, since a lot of commands treat space as separator (e.g. file names when using 'cp').

This command simply dumps everything into the terminal output

    cat my_file.txt

Another (more elegant) way to view a file is with what's called pager

    less my_file.txt

You can navigate the same way as using the man command (move with arrow keys, press Q to quit). In fact, the 'man' command depends on 'less' to display its content.


## Comparison

To compare two different files, you can use the diff command

First you see that both files from above are identical

    diff my_file.txt your_file.txt

Now let's make a change to one of them

    echo 'aaaaaaa' >> your_file.txt

Feel free to view the changed file

Compare it again

    diff my_file.txt your_file.txt

What do you see?

With the output of diff command, you can generate a patch. A patch is simply a document containing modifications done to certain file or directory. A program called 'patch' can be used to automatically apply this change.


## Regular Expression

A regular expression (regex) is simply a pattern that describes set of string. By itself, it is a mathematical concept, part of what's called formal language theory.

We use it to search for matching text. A program called 'grep' is available under most systems.

Let us begin by searching files with certain matching text

    grep 'aaa' my_file.txt
    grep 'aaa' your_file.txt

Do you see anything? By the way, you can type the above as a single command.

Search for any character after a space

    grep ' .\*' your_file.txt

For more information

    man 7 regex


## Stream Editing

You can use your normal text editor to edit your file (command line text editor is a thing!), but you cannot (conveniently) program that to edit your files automatically (e.g. in scripts). The sed command loads a string of text and modify it according to instructions.

One of the popular instruction is to find and replace matching text. We can find the match using regular expression, but for simplicity we use plain text.

To replace 'a' with 'b' on 'your_file.txt'

    sed 's/a/b/' -i your_file.txt

Append 'g' on the sed expression if there's multiple on the same line

    sed 's/a/c/g' -i your_file.txt

Each instruction given to 'sed' command is called a 'sed expression'. So far we've only been using one at a time. When we're dealing with multiple expression, each one is denoted with '-e'

    sed -e 's/b/a/g' -e 's/c/a/g' -i your_file.txt

Regular expression can be used too. In this case we're replacing every single letter with 'x'. The symbol dot ('.') in regex means any single character.

    sed 's/./x/g' -i error.txt

You can also delete lines with matching text, also with regular expression

    sed '/aaaa/d' -i your_file.txt


## I/O Redirection

One benefit of treating everything as files is that it makes it trivial to transfer from terminal to disk, or from one device to another -- it's just like transferring from one file to another!

First, check your directory

Save terminal output to file

    echo 'print this' > my_file.txt

Notice that you don't see the output; check your directory for a new file

Try it again!

    ls / > another_file.txt
    ls nonexistent_directory > error.txt

Notice that error.txt is empty -- the printed error message does not go to the file

Normal outputs (stdout) and error messages (stderr) are treated differently

To redirect error message

    ls nonexistent_directory 2> error.txt

By typing '2>', you are telling terminal to redirect stderr instead of stdout. Why the number two? Because that is the file descriptor ID of stderr.

Single arrow ('>') creates new file from scratch, destroying any existing file. Double arrow ('>>') appends on the end if target file exists.

    echo 'this one is appended!' >> my_file.txt

Notice that you can redirect this output into another file, just like any command

    cat my_file.txt > your_file.txt

Don't forget to check your directory again!

Pipe takes an output of a program and use it directly as another program's input. It uses the bar/pipe ('|') symbol, and takes the syntax of normal redirection, except that instead of file name, you put another command at the end.

    ls / | less

Let's try another example. This command lists all the running processes.

    ps -e

Now open Firefox from your program menu.

Then on your terminal, type:

    ps -e | grep firefox

This command uses 'grep' on the output of 'ps -e' to find match.

You should see some output matching your "search".

For more about the command 'ps', check out chapter A2, Process Runtime.


## Hash & Checksum

Sometimes files got damaged -- even worse, sometimes they got tampered. On some occasions, you can tell by the size (e.g. when download fails); but it doesn't always help.

We can use checksum: it takes a "signature" of given file at given instance. This way, we can check the integrity of our data -- when a file gets changed, its signature would too.

Let us demonstrate! First, to check the sum of a file

    md5sum my_file.txt

Feel free to run it a few more times -- the output will remain the same.

Now let's create a slightly modified copy of the file

    cp my_file.txt your_file.txt
    sed 's/a/c/g' -i your_file.txt

Check for the size

    ls -l

The checksum commands can be used on multiple files

    md5sum my_file.txt your_file.txt

Notice that despite the same file size, the checksum of the files are different.
