Command Line Guidance
=====================

[![license](by-nc.png)](https://creativecommons.org/licenses/by-nc/4.0/) **Timothy Kenno Handojo**

People think that only wizards can type a bunch of letters into a single line and make stuff appear. Here, we serve to show that you can do that too!

So far, we have:

[00 - Cheat Sheet](curriculum/00-cheatsheet.md)  
what one may need to know in order to begin

[A0 - File Management](curriculum/a0-file_management.md)  
how directory entries and file informations are handled
- Basic Operations
- Permission & Ownership
- File Sizes
- Timestamp
- Links
- File Types

[A1 - Text Processing](curriculum/a1-text_processing.md)  
how text content are handled within the input/output as well as files
- The UNIX I/O
- Comparison
- Regular Expression
- Stream Editing
- I/O Redirection
- Hash & Checksum

[A2 - Process Runtime](curriculum/a2-process_runtime.md)  
how running processes are managed by operating system and specific tools
- Boot Process
- Init Process
- System Time
- Runtime Information
- Process Communication
- Shell Variable

[B0 - Super Usage](curriculum/b0-super_usage.md)  
how privilege for different programs are defined
- Virtualization
- Package Manager
- System Daemons
- System Settings
- Log Files
- Switching User
- File Access
