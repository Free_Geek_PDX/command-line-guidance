Hash Algorithm
==============

Checksum, such as MD5, relies on what is called hash algorithm.

The main properties of these algorithms are:
* asymmetric -- cannot recreate the input from the output
* pseudo-random -- slight change on the input dramatically changes the output

This is how your data gets secured, one way or another -- encryptions too!

Primitive example: modulus/remainder operation
* n / 4 = **25** ⇒ **25** * 4 = n ⇒ n = 100 ; thieves can figure out what n is; easy!
* n % 6 = **4** ⇒ **4** + (6 * m) = n ; n could be anything, as we don't know m
